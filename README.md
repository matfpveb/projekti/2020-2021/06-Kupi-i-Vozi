# Kupi-i-Vozi

"Kupi i vozi" is a web app which allows you to buy and sell used or new cars. It has advanced searches and possibility to save favourite listings.  
- [Link to video about the project](https://www.youtube.com/watch?v=a6k5sssEIUw)
## Links to other doc files

- [Build procedure](https://gitlab.com/matfpveb/projekti/2020-2021/06-Kupi-i-Vozi/-/wikis/Build-procedure)
- [Persistent data description](https://gitlab.com/matfpveb/projekti/2020-2021/06-Kupi-i-Vozi/-/blob/master/documentation/ERDiagram.png)
- [General project setup](https://gitlab.com/matfpveb/projekti/2020-2021/06-Kupi-i-Vozi/-/wikis/General-project-setup)

## Developers

- [Luka Vujčić, 63/2017](https://gitlab.com/LukaVujcic)
- [Božidar Mitrović, 54/2017](https://gitlab.com/wade_wilson)
- [Dušan Petrović, 14/2017](https://gitlab.com/dpns98)
- [Aleksa Tešić, 121/2017](https://gitlab.com/Imafikus)
- [Mina Milošević, 81/2017](https://gitlab.com/mina.milosevic)
